FROM openjdk:11
EXPOSE 8092
RUN mkdir /university
RUN cd /university
RUN mkdir /books
RUN mkdir /chats
ADD target/FileSharingMicroservice-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]