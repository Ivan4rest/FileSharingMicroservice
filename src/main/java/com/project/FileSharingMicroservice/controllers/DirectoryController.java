package com.project.FileSharingMicroservice.controllers;

import com.project.FileSharingMicroservice.dto.FileDTO;
import com.project.FileSharingMicroservice.dto.FileMetadataDTO;
import com.project.FileSharingMicroservice.services.DirectoryService;
import com.project.FileSharingMicroservice.services.FileService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class DirectoryController {
    private FileService fileService;
    private DirectoryService directoryService;

    public FileService getFileService() {
        return fileService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public DirectoryService getDirectoryService() {
        return directoryService;
    }

    @Autowired
    public void setDirectoryService(DirectoryService directoryService) {
        this.directoryService = directoryService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    private String getUserUUID(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getSubject();
    }

    @GetMapping("/v0/directories/{directoryId}")
    public ResponseEntity<List<FileDTO>> getFilesByDirectoryId(@PathVariable Long directoryId,
                                                          HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<FileDTO> fileDTOS = getFileService().getFilesByDirectoryId(directoryId);
            Boolean isAvailable = true;
            for (FileDTO fileDTO:fileDTOS) {
                if(!getUserUUID(request).equals(fileDTO.getUserUUID()) && !fileDTO.getVisibility()){
                    isAvailable = false;
                }
            }
            if (getRoles(request).contains("ROLE_admin") || isAvailable){
                return new ResponseEntity<>(fileDTOS, headers, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/directories/{directoryId}/metadata")
    public ResponseEntity<List<FileMetadataDTO>> getFilesMetadataByDirectory(@PathVariable Long directoryId,
                                                                             HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<FileMetadataDTO> fileMetadataDTOS = getFileService().getFilesMetadataByDirectoryId(directoryId);
            Boolean isAvailable = true;
            for (FileMetadataDTO fileMetadataDTO:fileMetadataDTOS) {
                if(!getUserUUID(request).equals(fileMetadataDTO.getUserUUID()) && !fileMetadataDTO.getVisibility()){
                    isAvailable = false;
                }
            }
            if (getRoles(request).contains("ROLE_admin") || isAvailable){
                return new ResponseEntity<>(fileMetadataDTOS, headers, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/directories/{directoryId}/filenames/{filename}")
    public ResponseEntity<FileDTO> getFileByDirectoryIdAndFilename(@PathVariable Long directoryId,
                                                                     @PathVariable String filename,
                                                                     HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            FileDTO fileDTO = getFileService().getFileByDirectoryIdAndFilename(directoryId, filename);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileDTO.getUserUUID()) || fileDTO.getVisibility()){
                return new ResponseEntity<>(fileDTO, headers, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/v0/directories/{directoryId}")
    public ResponseEntity<?> deleteDirectoryById(@PathVariable Long directoryId,
                                                 HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<FileMetadataDTO> fileMetadataDTOS = getFileService().getFilesMetadataByDirectoryId(directoryId);
            Boolean isAvailable = true;
            for (FileMetadataDTO fileMetadataDTO:fileMetadataDTOS) {
                if(!getUserUUID(request).equals(fileMetadataDTO.getUserUUID())){
                    isAvailable = false;
                }
            }
            if(getRoles(request).contains("ROLE_admin") || isAvailable){
                getDirectoryService().deleteById(directoryId);
                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }
}
