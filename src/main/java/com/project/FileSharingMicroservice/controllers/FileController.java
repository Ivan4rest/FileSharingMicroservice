package com.project.FileSharingMicroservice.controllers;

import com.project.FileSharingMicroservice.dto.FileDTO;
import com.project.FileSharingMicroservice.dto.FileMetadataDTO;
import com.project.FileSharingMicroservice.services.FileService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class FileController {

    private FileService fileService;

    public FileService getFileService() {
        return fileService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    private String getUserUUID(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getSubject();
    }

    @GetMapping("/v0/files/{fileId}")
    public ResponseEntity<FileDTO> getFileById(@PathVariable Long fileId,
                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            FileDTO fileDTO = getFileService().getFileById(fileId);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileDTO.getUserUUID()) || fileDTO.getVisibility()){
                return new ResponseEntity<>(fileDTO, headers, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/files/{fileId}/metadata")
    public ResponseEntity<FileMetadataDTO> getFileMetadataById(@PathVariable Long fileId,
                                                           HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            FileMetadataDTO fileMetadataDTO = getFileService().getFileMetadataById(fileId);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileMetadataDTO.getUserUUID()) || fileMetadataDTO.getVisibility()){
                return new ResponseEntity<>(fileMetadataDTO, headers, HttpStatus.OK);
            }
            else {
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/v0/files")
    public ResponseEntity<?> addFile(@RequestBody FileDTO fileDTO,
                                     HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            return new ResponseEntity<>(getFileService().addFile(fileDTO, getUserUUID(request)), headers, HttpStatus.CREATED);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/v0/files/books")
    public ResponseEntity<?> addBookFile(@RequestBody FileDTO fileDTO,
                                         HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            return new ResponseEntity<>(getFileService().addBook(fileDTO, getUserUUID(request)), headers, HttpStatus.CREATED);
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/files/{fileId}")
    public ResponseEntity<?> updateFile(@PathVariable Long fileId,
                                        @RequestBody FileDTO fileDTO,
                                        HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileDTO.getUserUUID())){
                getFileService().updateFile(fileId, fileDTO);
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping("/v0/files/{fileId}/metadata")
    public ResponseEntity<?> updateFileMetadata(@PathVariable Long fileId,
                                                @RequestBody FileMetadataDTO fileMetadataDTO,
                                                HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileMetadataDTO.getUserUUID())){
                getFileService().updateFileMetadata(fileId, fileMetadataDTO);
                return new ResponseEntity<>(headers, HttpStatus.CREATED);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping("/v0/files/{fileId}")
    public ResponseEntity<?> deleteFileById(@PathVariable Long fileId,
                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            FileMetadataDTO fileMetadataDTO = getFileService().getFileMetadataById(fileId);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(fileMetadataDTO.getUserUUID())){
                getFileService().deleteFileById(fileId);
                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }
}
