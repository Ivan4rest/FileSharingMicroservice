package com.project.FileSharingMicroservice.controllers;

import com.project.FileSharingMicroservice.dto.FileDTO;
import com.project.FileSharingMicroservice.dto.FileMetadataDTO;
import com.project.FileSharingMicroservice.services.FileMetadataUserService;
import com.project.FileSharingMicroservice.services.FileService;
import org.keycloak.KeycloakSecurityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class UserController {
    private FileService fileService;
    private FileMetadataUserService fileMetadataUserService;

    public FileService getFileService() {
        return fileService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public FileMetadataUserService getFileMetadataUserService() {
        return fileMetadataUserService;
    }

    @Autowired
    public void setFileMetadataUserService(FileMetadataUserService fileMetadataUserService) {
        this.fileMetadataUserService = fileMetadataUserService;
    }

    private String getToken(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getTokenString();
    }

    private Set<String> getRoles(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getRealmAccess().getRoles();
    }

    private String getUserUUID(HttpServletRequest request)
    {
        KeycloakSecurityContext context = (KeycloakSecurityContext) request.getAttribute(KeycloakSecurityContext.class.getName());
        return context.getToken().getSubject();
    }


    @GetMapping("/v0/users/{userUUID}/files")
    public ResponseEntity<List<FileDTO>> getFilesByUserUUID(@PathVariable String userUUID,
                                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<FileDTO> fileDTOS = getFileService().getFilesByUserUUID(userUUID);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                return new ResponseEntity<>(fileDTOS, headers, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }

        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/v0/users/{userUUID}/files/metadata")
    public ResponseEntity<List<FileMetadataDTO>> getFilesMetadataByUserUUID(@PathVariable String userUUID,
                                                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            List<FileMetadataDTO> fileMetadataDTOS = getFileService().getFilesMetadataByUserUUID(userUUID);
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                return new ResponseEntity<>(fileMetadataDTOS, headers, HttpStatus.OK);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/v0/users/{userUUID}/files")
    public ResponseEntity<?> deleteFileById(@PathVariable String userUUID,
                                            HttpServletRequest request){
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Bearer " + getToken(request));
        try{
            if(getRoles(request).contains("ROLE_admin") || getUserUUID(request).equals(userUUID)){
                getFileMetadataUserService().deleteByUserUUID(userUUID);
                return new ResponseEntity<>(headers, HttpStatus.NO_CONTENT);
            }
            else{
                return new ResponseEntity<>(headers, HttpStatus.BAD_REQUEST);
            }
        }
        catch (Exception e){
            return new ResponseEntity<>(headers, HttpStatus.NOT_FOUND);
        }
    }
}
