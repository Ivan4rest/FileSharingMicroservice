package com.project.FileSharingMicroservice.services;

import com.project.FileSharingMicroservice.entities.Directory;
import com.project.FileSharingMicroservice.repositories.DirectoryRepository;
import com.project.FileSharingMicroservice.repositories.FileMetadataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class DirectoryService {
    private DirectoryRepository directoryRepository;
    private FileMetadataRepository fileMetadataRepository;
    private FileService fileService;

    @Value("${storage.path}")
    private String storagePath;

    public DirectoryRepository getDirectoryRepository() {
        return directoryRepository;
    }

    @Autowired
    public void setDirectoryRepository(DirectoryRepository directoryRepository) {
        this.directoryRepository = directoryRepository;
    }

    public FileMetadataRepository getFileMetadataRepository() {
        return fileMetadataRepository;
    }

    @Autowired
    public void setFileMetadataRepository(FileMetadataRepository fileMetadataRepository) {
        this.fileMetadataRepository = fileMetadataRepository;
    }

    public FileService getFileService() {
        return fileService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public List<Directory> getAll(){
        return getDirectoryRepository().findAll();
    }

    public Directory getById(Long id){
        return getDirectoryRepository().findById(id).get();
    }

    public Long getDirectoryByPath(String directoryPath){
        return getDirectoryRepository().getDirectoryByPath(directoryPath);
    }

    @Transactional
    public Directory add(Directory directory){
        return getDirectoryRepository().save(directory);
    }

    @Transactional
    public Directory update(Long id, Directory directory){
        Directory directoryUpdate = getDirectoryRepository().findById(id).get();
        directoryUpdate.setPath(directory.getPath());
        return getDirectoryRepository().save(directoryUpdate);
    }

    @Transactional
    public void deleteById(Long id) throws IOException {
        List<Long> filesId = getFileMetadataRepository().getFilesMetadataIdByDirectoryId(id);
        Directory directory = getDirectoryRepository().findById(id).get();
        File storageDir = new File(storagePath + "/" + directory.getPath());
        if(!storageDir.exists()){
            throw new IOException();
        }
        for (Long fileId:filesId) {
            getFileService().deleteFileById(fileId);
        }
        if(!storageDir.delete()){
            throw new IOException();
        }
        getDirectoryRepository().deleteById(id);
    }
}
