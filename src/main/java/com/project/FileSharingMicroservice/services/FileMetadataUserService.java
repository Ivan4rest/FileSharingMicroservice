package com.project.FileSharingMicroservice.services;

import com.project.FileSharingMicroservice.entities.FileMetadataUser;
import com.project.FileSharingMicroservice.repositories.FileMetadataUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.List;

@Service
public class FileMetadataUserService {
    private FileMetadataUserRepository fileMetadataUserRepository;
    private FileService fileService;

    public FileMetadataUserRepository getFileMetadataUserRepository() {
        return fileMetadataUserRepository;
    }

    @Autowired
    public void setFileMetadataUserRepository(FileMetadataUserRepository fileMetadataUserRepository) {
        this.fileMetadataUserRepository = fileMetadataUserRepository;
    }

    public FileService getFileService() {
        return fileService;
    }

    @Autowired
    public void setFileService(FileService fileService) {
        this.fileService = fileService;
    }

    public List<FileMetadataUser> getAll(){
        List<FileMetadataUser> fileMetadataUsers = getFileMetadataUserRepository().findAll();
        return fileMetadataUsers;
    }

    public FileMetadataUser getById(Long id){
        FileMetadataUser fileMetadataUser = getFileMetadataUserRepository().findById(id).get();
        return fileMetadataUser;
    }

    public List<String> getUsersIdByFileMetadataId(Long fileMetadataId){
        List<String> usersUUID = getFileMetadataUserRepository().getByFileMetadataId(fileMetadataId);
        return usersUUID;
    }

    public List<Long> getFilesMetadataIdByUserUUID(String userUUID){
        List<Long> filesMetadataId = getFileMetadataUserRepository().getByUserUUID(userUUID);
        return filesMetadataId;
    }

    @Transactional
    public void add(FileMetadataUser fileMetadataUser){
        getFileMetadataUserRepository().save(fileMetadataUser);
    }

    @Transactional
    public void deleteById(Long id){
        getFileMetadataUserRepository().deleteById(id);
    }

    @Transactional
    public void deleteByFileMetadataId(Long fileMetadataId){
        getFileMetadataUserRepository().deleteByFileMetadataId(fileMetadataId);
    }

    @Transactional
    public void deleteByUserUUID(String userUUID) throws IOException {
        List<Long> filesMetadataId = getFileMetadataUserRepository().getByUserUUID(userUUID);
        for (Long fileMetadataId:filesMetadataId) {
            getFileService().deleteFileById(fileMetadataId);
        }
    }
}
