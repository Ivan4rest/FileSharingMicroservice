package com.project.FileSharingMicroservice.services;

import com.project.FileSharingMicroservice.dto.FileDTO;
import com.project.FileSharingMicroservice.dto.FileMetadataDTO;
import com.project.FileSharingMicroservice.entities.Directory;
import com.project.FileSharingMicroservice.entities.FileMetadata;
import com.project.FileSharingMicroservice.entities.FileMetadataUser;
import com.project.FileSharingMicroservice.repositories.FileMetadataRepository;
import org.apache.commons.io.IOUtils;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Service
public class FileService {

    private FileMetadataRepository fileMetadataRepository;
    private DirectoryService directoryService;
    private FileMetadataUserService fileMetadataUserService;

    @Value("${storage.path}")
    private String storagePath;

    public FileMetadataRepository getFileMetadataRepository() {
        return fileMetadataRepository;
    }

    @Autowired
    public void setFileMetadataRepository(FileMetadataRepository fileMetadataRepository) {
        this.fileMetadataRepository = fileMetadataRepository;
    }

    public DirectoryService getDirectoryService() {
        return directoryService;
    }

    @Autowired
    public void setDirectoryService(DirectoryService directoryService) {
        this.directoryService = directoryService;
    }

    public FileMetadataUserService getFileMetadataUserService() {
        return fileMetadataUserService;
    }

    @Autowired
    public void setFileMetadataUserService(FileMetadataUserService fileMetadataUserService) {
        this.fileMetadataUserService = fileMetadataUserService;
    }

    public FileDTO getFileById(Long id) throws IOException {
        FileMetadata fileMetadata = getFileMetadataRepository().findById(id).get();
        File storageDir = new File(storagePath + "/" + fileMetadata.getDirectory().getPath());
        if(!storageDir.exists()){
            throw new IOException();
        }
        File file = new File(storageDir.getPath() + "/" + fileMetadata.getFilename());
        if(!file.exists()){
            throw new IOException();
        }
        FileInputStream fileInputStream = new FileInputStream(file);
        return new FileDTO(fileMetadata, IOUtils.toByteArray(fileInputStream));
    }

    public FileMetadataDTO getFileMetadataById(Long id){
        FileMetadata fileMetadata = getFileMetadataRepository().findById(id).get();
        return new FileMetadataDTO(fileMetadata);
    }

    public List<FileDTO> getFilesByDirectoryId(Long directoryId) throws IOException {
        List<FileDTO> fileDTOS = new ArrayList<>();
        Directory directory = getDirectoryService().getById(directoryId);
        List<FileMetadata> filesMetadata = getFileMetadataRepository().getFilesMetadataByDirectoryId(directoryId);
        for (FileMetadata fileMetadata: filesMetadata) {
            File storageDir = new File(storagePath + "/" + directory.getPath());
            if(!storageDir.exists()){
                throw new IOException();
            }
            File fileSys = new File(storageDir.getPath() + "/" + fileMetadata.getFilename());
            if(!fileSys.exists()){
                throw new IOException();
            }
            FileInputStream fileInputStream = new FileInputStream(fileSys);
            fileDTOS.add(new FileDTO(fileMetadata, IOUtils.toByteArray(fileInputStream)));
        }
        return fileDTOS;
    }

    public List<FileMetadataDTO> getFilesMetadataByDirectoryId(Long directoryId){
        List<FileMetadataDTO> fileMetadataDTOS = new ArrayList<>();
        List<Long> filesMetadataId = getFileMetadataRepository().getFilesMetadataIdByDirectoryId(directoryId);
        for (Long fileMetadataId : filesMetadataId) {
            fileMetadataDTOS.add(new FileMetadataDTO(getFileMetadataRepository().findById(fileMetadataId).get()));
        }
        return fileMetadataDTOS;
    }

    public FileDTO getFileByDirectoryIdAndFilename(Long directoryId, String filename) throws IOException {
        Long fileId = getFileMetadataRepository().getFileMetadataIdByDirectoryIdAndFilename(directoryId, filename);
        return getFileById(fileId);
    }

    public List<FileDTO> getFilesByUserUUID(String userUUID) throws IOException {
        List<FileDTO> fileDTOS = new ArrayList<>();
        List<Long> filesMetadataId = getFileMetadataUserService().getFilesMetadataIdByUserUUID(userUUID);
        for (Long fileMetadataId:filesMetadataId) {
            fileDTOS.add(getFileById(fileMetadataId));
        }
        return fileDTOS;
    }

    public List<FileMetadataDTO> getFilesMetadataByUserUUID(String userUUID){
        List<FileMetadataDTO> fileMetadataDTOS = new ArrayList<>();
        List<Long> filesMetadataId = getFileMetadataUserService().getFilesMetadataIdByUserUUID(userUUID);
        for (Long fileMetadataId : filesMetadataId) {
            fileMetadataDTOS.add(getFileMetadataById(fileMetadataId));
        }
        return fileMetadataDTOS;
    }

    @Transactional
    public FileMetadataDTO addFile(FileDTO fileDTO, String userUUID) throws IOException {
        Long directoryId = getDirectoryService().getDirectoryByPath(fileDTO.getDirectoryPath());
        if(directoryId == null){
            directoryId = getDirectoryService().add(new Directory(fileDTO.getDirectoryPath())).getId();
        }
        FileMetadata fileMetadata = new FileMetadata(fileDTO.getAddedDate(), fileDTO.getFilename(), fileDTO.getVisibility(), new Directory(directoryId, fileDTO.getDirectoryPath()));
        fileMetadata = getFileMetadataRepository().save(fileMetadata);
        getFileMetadataUserService().add(new FileMetadataUser(userUUID, fileMetadata));
        if(fileDTO.getFileInBytes() != null){
            File storageDir = new File(storagePath + "/" + fileDTO.getDirectoryPath());
            if(!storageDir.exists()){
                if(!storageDir.mkdir()){
                    throw new IOException();
                }
            }
            File convertFile = new File(storageDir.getPath() + "/" + fileDTO.getFilename());
            if(!convertFile.createNewFile()){
                throw new IOException();
            }
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(fileDTO.getFileInBytes());
            fout.close();
        }
        return getFileMetadataById(fileMetadata.getId());
    }

    @Transactional
    public FileMetadataDTO addBook(FileDTO fileDTO, String userUUID) throws IOException {
        FileMetadataDTO fileMetadataDTO = addFile(fileDTO, userUUID);
        Date currentDate = new Date(new java.util.Date().getTime());
        PDDocument document = PDDocument.load(fileDTO.getFileInBytes());
        PDFRenderer pdfRenderer = new PDFRenderer(document);
        for (int i = 0; i < document.getNumberOfPages(); i++) {
            BufferedImage bim = pdfRenderer.renderImageWithDPI(i, 300, ImageType.RGB);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(bim, "png", baos);
            byte[] bytes = baos.toByteArray();
            FileDTO fileDTOImg = new FileDTO(currentDate, true, fileDTO.getDirectoryPath(), i + ".png", bytes);
            addFile(fileDTOImg, userUUID);
        }
        document.close();
        return fileMetadataDTO;
    }

    @Transactional
    public void updateFile(Long id, FileDTO fileDTO) throws IOException {
        FileMetadata fileMetadataUpdate = getFileMetadataRepository().findById(id).get();
        fileMetadataUpdate.setAddedDate(fileDTO.getAddedDate());
        fileMetadataUpdate.setVisibility(fileDTO.getVisibility());
        fileMetadataUpdate.setDirectory(new Directory(fileDTO.getDirectoryId(), fileDTO.getDirectoryPath()));
        fileMetadataUpdate.setFilename(fileDTO.getFilename());
        getFileMetadataRepository().save(fileMetadataUpdate);
        if(fileDTO.getFileInBytes() != null){
            File storageDir = new File(storagePath + "/" + fileDTO.getDirectoryPath());
            if(!storageDir.exists()){
                throw new IOException();
            }
            File convertFile = new File(storageDir.getPath() + "/" + fileDTO.getFilename());
            if(!convertFile.exists()){
                throw new IOException();
            }
            convertFile.createNewFile();
            FileOutputStream fout = new FileOutputStream(convertFile);
            fout.write(fileDTO.getFileInBytes());
            fout.close();
        }
    }

    @Transactional
    public void updateFileMetadata(Long id, FileMetadataDTO fileMetadataDTO){
        FileMetadata fileMetadataUpdate = getFileMetadataRepository().findById(id).get();
        fileMetadataUpdate.setVisibility(fileMetadataDTO.getVisibility());
        getFileMetadataRepository().save(fileMetadataUpdate);
    }

    @Transactional
    public void deleteFileById(Long id) throws IOException {
        FileMetadata fileMetadata = getFileMetadataRepository().findById(id).get();
        Directory directory = getDirectoryService().getById(fileMetadata.getDirectory().getId());
        File storageDir = new File(storagePath + "/" + directory.getPath());
        if(!storageDir.exists()){
            throw new IOException();
        }
        File fileSys = new File(storageDir.getPath() + "/" + fileMetadata.getFilename());
        if(!fileSys.exists()){
            throw new IOException();
        }
        if(!fileSys.delete()){
            throw new IOException();
        }
        getFileMetadataUserService().deleteByFileMetadataId(id);
        getFileMetadataRepository().deleteById(id);
    }
}
