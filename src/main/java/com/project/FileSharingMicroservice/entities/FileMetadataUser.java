package com.project.FileSharingMicroservice.entities;

import javax.persistence.*;

@Entity
@Table(name = "file_metadata_users")
public class FileMetadataUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column(name = "user_uuid")
    private String userUUID;

    @ManyToOne(cascade = CascadeType.ALL,
            fetch = FetchType.EAGER)
    @JoinColumn(name = "file_metadata_id")
    private FileMetadata fileMetadata;

    public FileMetadataUser() {
    }

    public FileMetadataUser(String userUUID, FileMetadata fileMetadata) {
        this.userUUID = userUUID;
        this.fileMetadata = fileMetadata;
    }

    public Long getId() {
        return id;
    }

    public String getUserUUID() {
        return userUUID;
    }

    public void setUserUUID(String userUUID) {
        this.userUUID = userUUID;
    }

    public FileMetadata getFileMetadata() {
        return fileMetadata;
    }

    public void setFileMetadata(FileMetadata fileMetadata) {
        this.fileMetadata = fileMetadata;
    }
}
