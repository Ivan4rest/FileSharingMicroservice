package com.project.FileSharingMicroservice.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.sql.Date;
import java.util.Set;

@Entity
@Table(name = "files_metadata")
public class FileMetadata {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    @NotNull(message = "Added date is mandatory")
    private Date addedDate;

    @Column
    @NotBlank(message = "Filename is mandatory")
    private String filename;

    @Column
    @NotNull(message = "Visibility is mandatory")
    private Boolean visibility;

    @ManyToOne
    @JoinColumn(name = "directory_id")
    private Directory directory;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "fileMetadata")
    private Set<FileMetadataUser> fileMetadataUsers;

    public FileMetadata() {
    }

    public FileMetadata(Long id, Date addedDate, String filename, Boolean visibility, Directory directory) {
        this.id = id;
        this.addedDate = addedDate;
        this.filename = filename;
        this.visibility = visibility;
        this.directory = directory;
    }

    public FileMetadata(Date addedDate, String filename, Boolean visibility, Directory directory) {
        this.addedDate = addedDate;
        this.filename = filename;
        this.visibility = visibility;
        this.directory = directory;
    }

    public Long getId() {
        return id;
    }

    public Date getAddedDate() {
        return addedDate;
    }

    public void setAddedDate(Date addedDate) {
        this.addedDate = addedDate;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public Boolean getVisibility() {
        return visibility;
    }

    public void setVisibility(Boolean visibility) {
        this.visibility = visibility;
    }

    public Directory getDirectory() {
        return directory;
    }

    public void setDirectory(Directory directory) {
        this.directory = directory;
    }

    public Set<FileMetadataUser> getFileMetadataUsers() {
        return fileMetadataUsers;
    }

    public void setFileMetadataUsers(Set<FileMetadataUser> fileMetadataUsers) {
        this.fileMetadataUsers = fileMetadataUsers;
    }
}
