package com.project.FileSharingMicroservice.entities;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.util.Set;

@Entity
@Table(name = "directories")
public class Directory {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column
    private Long id;

    @Column
    @NotBlank(message = "Path is mandatory")
    private String path;

    @OneToMany(cascade = CascadeType.ALL,
            fetch = FetchType.LAZY,
            mappedBy = "directory")
    private Set<FileMetadata> filesMetadata;

    public Directory() {
    }

    public Directory(String path) {
        this.path = path;
    }

    public Directory(Long id, String path) {
        this.id = id;
        this.path = path;
    }

    public Long getId() {
        return id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Set<FileMetadata> getFilesMetadata() {
        return filesMetadata;
    }

    public void setFilesMetadata(Set<FileMetadata> filesMetadata) {
        this.filesMetadata = filesMetadata;
    }
}
