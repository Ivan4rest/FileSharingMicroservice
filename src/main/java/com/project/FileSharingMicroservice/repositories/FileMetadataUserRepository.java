package com.project.FileSharingMicroservice.repositories;

import com.project.FileSharingMicroservice.entities.FileMetadataUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface FileMetadataUserRepository extends JpaRepository<FileMetadataUser, Long> {

    @Query("select fmu.fileMetadata.id from FileMetadataUser fmu where fmu.userUUID = ?1")
    List<Long> getByUserUUID(String userUUID);

    @Query("select fmu.userUUID from FileMetadataUser fmu where fmu.fileMetadata.id = ?1")
    List<String> getByFileMetadataId(Long fileMetadataId);

    @Transactional
    @Modifying
    @Query("delete from FileMetadataUser fmu where fmu.fileMetadata.id = ?1")
    void deleteByFileMetadataId(Long fileMetadataId);

    @Transactional
    @Modifying
    @Query("delete from FileMetadataUser fmu where fmu.userUUID = ?1")
    void deleteByUserUUID(String userUUID);
}
