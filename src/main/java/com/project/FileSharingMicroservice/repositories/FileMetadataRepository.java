package com.project.FileSharingMicroservice.repositories;

import com.project.FileSharingMicroservice.entities.FileMetadata;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface FileMetadataRepository extends JpaRepository<FileMetadata, Long> {

    @Query("select fm.id, fm.filename, fm.addedDate, fm.visibility, fm.directory.id, fm.directory.path from FileMetadata fm where fm.directory.id = ?1")
    List<FileMetadata> getFilesMetadataByDirectoryId(Long directoryId);

    @Query("select fm.id from FileMetadata fm where fm.directory.id = ?1")
    List<Long> getFilesMetadataIdByDirectoryId(Long directoryId);

    @Query("select fm.id from FileMetadata fm where fm.directory.id = ?1 and fm.filename = ?2")
    Long getFileMetadataIdByDirectoryIdAndFilename(Long directoryId, String filename);

    @Transactional
    @Modifying
    @Query("delete from FileMetadata fm where fm.directory.id = ?1")
    void deleteFilesMetadataByDirectoryId(Long directoryId);
}