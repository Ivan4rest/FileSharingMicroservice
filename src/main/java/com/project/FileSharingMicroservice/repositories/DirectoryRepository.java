package com.project.FileSharingMicroservice.repositories;

import com.project.FileSharingMicroservice.entities.Directory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectoryRepository extends JpaRepository<Directory, Long> {
    @Query("select d.id from Directory d where d.path = ?1")
    Long getDirectoryByPath(String directoryPath);
}
