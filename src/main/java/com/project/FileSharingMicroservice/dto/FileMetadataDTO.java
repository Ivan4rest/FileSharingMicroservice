package com.project.FileSharingMicroservice.dto;

import com.project.FileSharingMicroservice.entities.FileMetadata;
import lombok.Data;

import java.sql.Date;

@Data
public class FileMetadataDTO {
    Long id;
    Date addedDate;
    String userUUID;
    Boolean visibility;
    Long directoryId;
    String directoryPath;
    String filename;

    public FileMetadataDTO() {
    }

    public FileMetadataDTO(Long id, Date addedDate, String userUUID, Boolean visibility, Long directoryId, String directoryPath, String filename) {
        this.id = id;
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
    }

    public FileMetadataDTO(FileMetadata fileMetadata, String userUUID) {
        this.id = fileMetadata.getId();
        this.addedDate = fileMetadata.getAddedDate();
        this.userUUID = userUUID;
        this.visibility = fileMetadata.getVisibility();
        this.directoryId = fileMetadata.getDirectory().getId();
        this.directoryPath = fileMetadata.getDirectory().getPath();
        this.filename = fileMetadata.getFilename();
    }

    public FileMetadataDTO(FileMetadata fileMetadata) {
        this.id = fileMetadata.getId();
        this.addedDate = fileMetadata.getAddedDate();
        this.visibility = fileMetadata.getVisibility();
        this.directoryId = fileMetadata.getDirectory().getId();
        this.directoryPath = fileMetadata.getDirectory().getPath();
        this.filename = fileMetadata.getFilename();
    }
}
