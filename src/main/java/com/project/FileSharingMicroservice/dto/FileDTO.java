package com.project.FileSharingMicroservice.dto;

import com.project.FileSharingMicroservice.entities.FileMetadata;
import lombok.Data;

import java.sql.Date;

@Data
public class FileDTO {
    Long id;
    Date addedDate;
    String userUUID;
    Boolean visibility;
    Long directoryId;
    String directoryPath;
    String filename;
    byte[] fileInBytes;

    public FileDTO() {
    }

    public FileDTO(Long id, Date addedDate, String userUUID, Boolean visibility, Long directoryId, String directoryPath, String filename, byte[] fileInBytes) {
        this.id = id;
        this.addedDate = addedDate;
        this.userUUID = userUUID;
        this.visibility = visibility;
        this.directoryId = directoryId;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(Date addedDate, Boolean visibility, String directoryPath, String filename, byte[] fileInBytes) {
        this.addedDate = addedDate;
        this.visibility = visibility;
        this.directoryPath = directoryPath;
        this.filename = filename;
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(FileMetadata fileMetadata, byte[] fileInBytes) {
        this.id = fileMetadata.getId();
        this.addedDate = fileMetadata.getAddedDate();
        this.visibility = fileMetadata.getVisibility();
        this.directoryId = fileMetadata.getDirectory().getId();
        this.directoryPath = fileMetadata.getDirectory().getPath();
        this.filename = fileMetadata.getFilename();
        this.fileInBytes = fileInBytes;
    }

    public FileDTO(FileMetadata fileMetadata, String userUUID, byte[] fileInBytes) {
        this.id = fileMetadata.getId();
        this.addedDate = fileMetadata.getAddedDate();
        this.userUUID = userUUID;
        this.visibility = fileMetadata.getVisibility();
        this.directoryId = fileMetadata.getDirectory().getId();
        this.directoryPath = fileMetadata.getDirectory().getPath();
        this.filename = fileMetadata.getFilename();
        this.fileInBytes = fileInBytes;
    }
}
